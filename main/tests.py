from django.test import TestCase, RequestFactory
from django.shortcuts import get_object_or_404
from .models import Accounts,Top_Up_Type,User_Wallet,Package,Order,Truemoney,Question,Reply
import datetime
# from .views import index,getPackageList,getTopUpTypeID,list,buy,login_regis_view,do_login,do_regis,do_logout,history,get7code,getSteamCode,getPSCode,edit,topup,mainforum,postQuestion,viewQuestion,replyQuestion,deleteQuestion
# Create your tests here.
class TestCase(TestCase):

    def setUp(self):
        self.factory = RequestFactory()
        self.account1 = Accounts.objects.create(username='test1', email='test1@example.com', password='pass1' )
        self.wallet1 = User_Wallet.objects.create(user_id=self.account1.user_id, money=0, point=1)
        self.account2 = Accounts.objects.create(username='test2', email='test2@example.com', password='pass2' )
        self.wallet2 = User_Wallet.objects.create(user_id=self.account2.user_id, money=0, point=1)

        self.type1 = Top_Up_Type.objects.create(type_name='STEAM',image_path='../static/images/steam.png')
        self.type2 = Top_Up_Type.objects.create(type_name='LINE',image_path='../static/images/line.png')
        self.type3 = Top_Up_Type.objects.create(type_name='PLAY_STATION',image_path='../static/images/psn.png')

    def test_added_topup_type(self):
        topup_type = Top_Up_Type.objects.all()
        self.assertEqual(len(topup_type), 3)

    def test_added_wallet(self):
        wallets = User_Wallet.objects.all()
        self.assertEqual(len(wallets), 2)

    def test_added_account(self):
        accounts = Accounts.objects.all()
        self.assertEqual(len(accounts), 2)

    def test_package(self):
        topup_type = Top_Up_Type.objects.get(type_name='STEAM')
        self.package1 = Package.objects.create(top_up_type_id=topup_type, value=0, price=0, point=1)
        topup_type2 = Top_Up_Type.objects.get(type_name='LINE')
        self.package2 = Package.objects.create(top_up_type_id=topup_type2, value=0, price=0, point=1)
        topup_type3 = Top_Up_Type.objects.get(type_name='PLAY_STATION')
        self.package3 = Package.objects.create(top_up_type_id=topup_type3, value=0, price=0, point=1)
        packages = Package.objects.all()
        self.assertEqual(len(packages),3)

    # def test_order(self):
    #     user = Accounts.objects.get(username='test1')
    #     topup_type = Top_Up_Type.objects.get(type_name='STEAM')
    #     package = Package.objects.get(top_up_type_id=topup_type)
    #     self.order1 = Order.objects.create(user_id=user, top_up_type_id=top_up_type, package_id=package_id, date_order=datetime.date.today(), date_paid=datetime.date.today(), date_count=0, status='notpaid', top_up_code='64756-zhif0-b6iyj', payment_code='937268855508800544405850783932')
    #     order = Order.objects.all()
    #     self.assertEqual(len(order),1)

    def test_truemoney(self):
        self.truemoney = Truemoney.objects.create(value='20',card_id='11234567890')
        true = Truemoney.objects.all()
        self.assertEqual(len(true),1)

    def test_question(self):
        self.question = Question.objects.create(question_subject="subject1", question_detail="detail1", user_id='1',
        question_date=datetime.date.today())
        question = Question.objects.all()
        self.assertEqual(len(question),1)

    def test_reply(self):
        self.reply = Reply.objects.create(reply_detail="reply1", question_id="1", user_id='1', reply_date=datetime.date.today())
        reply = Reply.objects.all()
        self.assertEqual(len(reply),1)
