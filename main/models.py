import datetime
from django.db import models

class Accounts (models.Model) :
    user_id = models.AutoField(primary_key=True)
    username = models.CharField(max_length=15)
    email = models.CharField(max_length=30)
    password = models.CharField(max_length=20)

    def __str__(self):
        return u'id : %s ( %s )' % (self.user_id , self.username)

class Order (models.Model) :
    order_id = models.AutoField(primary_key=True)
    user_id = models.IntegerField()
    top_up_type_id = models.IntegerField()
    package_id = models.IntegerField()
    date_order = models.DateTimeField()
    date_paid = models.DateTimeField()
    date_count = models.IntegerField(3);
    status = models.CharField(max_length=10)
    top_up_code = models.CharField(max_length=100)
    payment_code = models.CharField(max_length=30)

    def __str__(self):
        return u'id : %s' % self.order_id

class User_Wallet (models.Model) :
    user_id = models.IntegerField();
    money = models.IntegerField();
    point = models.IntegerField(default=1);

class Package (models.Model) :
    package_id = models.AutoField(primary_key=True)
    top_up_type_id = models.ForeignKey('Top_Up_Type')
    value = models.IntegerField();
    price = models.IntegerField();
    point = models.IntegerField(default=1);

    def __str__(self):
        return u'id : %s' % self.package_id

class Top_Up_Type (models.Model) :
    top_up_type_id = models.AutoField(primary_key=True)
    TYPE_NAME_CHOICES = (
        ('STEAM','steam'),
        ('LINE','line'),
        ('PLAY_STATION','play_station')
    )
    type_name = models.CharField(max_length=20,choices=TYPE_NAME_CHOICES)
    image_path = models.CharField(max_length=100)

    def __str__(self):
        return u'id : %s ( %s )' % (self.top_up_type_id ,self.type_name)

class Truemoney (models.Model) :
    value = models.IntegerField();
    card_id = models.CharField(max_length=14)

class Question(models.Model) :
    question_id = models.AutoField(primary_key=True)
    question_subject = models.CharField(max_length=200)
    question_detail = models.CharField(max_length=1000)
    user_id = models.IntegerField()
    question_date = models.DateTimeField()

class Reply(models.Model) :
    reply_id = models.AutoField(primary_key=True)
    reply_detail = models.CharField(max_length=500)
    question_id = models.FloatField()
    user_id = models.IntegerField()
    reply_date = models.DateTimeField()
