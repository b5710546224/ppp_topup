from django.conf.urls import url
#from django.contrib import admin
from .import views

app_name = 'main'

urlpatterns = [
  url(r'^$', views.index, name='index'),
  url(r'^list', views.list, name='list'),
  url(r'^buy', views.buy, name='buy'),
  url(r'^login',views.login_regis_view,name='login_regis'),
  url(r'^doregis',views.do_regis,name='regis'),
  url(r'^dologin',views.do_login,name='login'),
  url(r'^history',views.history,name='history'),
  url(r'^logout',views.do_logout,name='logout'),
  url(r'^topup',views.topup,name='topup'),
  url(r'^mainforum/$',views.mainforum,name='mainforum'),
  url(r'^postQuestion/$',views.postQuestion,name='postQuestion'),
  url(r'^viewQuestion/$',views.viewQuestion,name='viewQuestion'),
  url(r'^replyQuestion/$',views.replyQuestion,name='replyQuestion'),
  url(r'^edit/$',views.edit,name='edit'),
  url(r'^deleteQuestion/$',views.deleteQuestion,name='deleteQuestion'),
]
