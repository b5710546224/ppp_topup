from django.contrib import admin

from .models import Accounts,Order,Package,Top_Up_Type,User_Wallet,Truemoney,Question,Reply

admin.site.register(Accounts)
admin.site.register(Order)
admin.site.register(Package)
admin.site.register(Top_Up_Type)
admin.site.register(User_Wallet)
admin.site.register(Truemoney)
admin.site.register(Question)
admin.site.register(Reply)
