from django.shortcuts import render
from main.models import Top_Up_Type,Package,Order,User_Wallet,Truemoney,Question,Reply
from .models import Accounts
from datetime import datetime
from django.http import HttpResponseRedirect
from django.urls import reverse
import random
# Create your views here.
def index(request) :
    if 'username' not in request.session:
        return render(request,'index.html',{'username':'','user_id':''})
    else:
        username = request.session[ 'username' ]
        user_id = request.session[ 'username' ]
        a = Top_Up_Type.objects.all()
        return render(request,'index.html',{'username':request.session[ 'username' ],'user_id':request.session[ 'id' ]})

def getPackageList(key) :
    if key == 'steam' :
      return  Package.objects.filter( top_up_type_id=int(1) )
    elif key == 'line':
      return  Package.objects.filter( top_up_type_id=int(2) )
    else:
      return  Package.objects.filter( top_up_type_id=int(3) )

def getTopUpTypeID(key) :
    if key == 'steam' :
      return  1
    elif key == 'line':
      return  2
    else:
      return  3

def list(request) :
    key = request.POST['key']
    package_list = getPackageList(key)
    if 'username' not in request.session:
        return render(request,'list.html',{'key':key,'x':package_list,'username':'','user_id':'','user':''})
    else:
        user = User_Wallet.objects.get(user_id=request.session['id'])
        return render(request,'list.html',{'key':key,'x':package_list,'username':request.session[ 'username' ],'user_id':request.session[ 'id' ],'error':'n','user':user})

def buy(request) :
    key = request.POST['key']
    package_list = getPackageList(key)
    top_up_type_id = getTopUpTypeID(key)
    if 'username' not in request.session:
        return render(request,'list.html',{'key':key,'x':package_list,'username':'','user_id':'','user':''})
    else:
        method = request.POST['method']
        package_id = request.POST['package_id']
        user_id = request.session[ 'id' ]
        price = request.POST['price']
        point = request.POST['point']

        payment_code = ''
        top_up_code = ''
        status="notpaid"
        current_point = User_Wallet.objects.get(user_id=request.session['id'])
        user = User_Wallet.objects.get(user_id=request.session['id'])
        if method == 'wallet':
            status="ready"
            top_up_code = getSteamCode()
            current_value = User_Wallet.objects.get(user_id=request.session['id']);
            package_value = Package.objects.get(package_id=package_id)
            package_point = Package.objects.get(package_id=package_id)
            if int(current_value.money) < int(package_value.price) :
                return render(request,'list.html',{'key':key,'x':package_list,'username':request.session[ 'username' ],'user_id':request.session[ 'id' ],'error':'y','user':user})
            else :
              new_value = int(current_value.money)-int(price)
              new_point = int(current_point.point)+int(package_point.point)
              User_Wallet.objects.filter( user_id=request.session['id']).update(money=new_value,point=new_point)
        else:
            status="notpaid"
            payment_code = get7code()

        db = Order(user_id=user_id, top_up_type_id=top_up_type_id, package_id=package_id, date_order=datetime.now() ,date_paid=datetime.now(),date_count='1',status=status,top_up_code=top_up_code,payment_code=payment_code)
        db.save()
        return history(request)

def login_regis_view( request ) :
    if 'username' not in request.session:
        return render(request, 'login_register_view.html',{'username':'','user_id':''})
    else:
        return render(request, 'login_register_view.html',{'username':request.session[ 'username' ],'user_id':request.session[ 'id' ]})

def do_login( request ) :
    if request.method == "POST" :
        _login_key = request.POST.get( "login_key" )
        _login_pass = request.POST.get( "login_password" )

        row = Accounts.objects.filter( username=_login_key, password=_login_pass )
        user = None
        if not row:
            row = Accounts.objects.filter( email=_login_key, password=_login_pass )

        if not row :
            request.session[ 'notice' ] = "user not found"
            return render( request, 'login_register_view.html')
        user = row[0]

        request.session[ 'username' ] = user.username
        request.session[ 'id' ] = user.user_id
        request.session[ 'email' ] = user.email

    print('test session : ')
    print(request.session[ 'id' ])
    print(request.session[ 'email' ])
    return render( request, 'index.html',{'username':request.session[ 'username' ],'user_id':request.session[ 'id' ]} )

def do_regis( request ):

    _username = request.POST['regis_username']
    _email = request.POST['regis_email']
    _pass = request.POST['regis_password']
    _con_pass = request.POST['regis_confirmpassword']

    if _pass == _con_pass :
        new_account = Accounts( username=_username, email=_email, password=_pass )
        new_account.save()
        _user_id = Accounts.objects.get( username = _username )

        new_wallet = User_Wallet( user_id=_user_id.user_id, money='0')
        new_wallet.save();

        request.session[ 'notice' ] = "Successful create accounts"
        request.session[ 'username' ] = new_account.username
        return render( request, "login_register_view.html" )
    else :
        request.session[ 'notice' ] = "Password incorrect"
        return render( request, "login_register_view.html" )

        return render( request, 'index.html' )
    # return render( request, 'index.html' )
    # return HttpResponseRedirect('/login/')

def do_logout ( request ):
    del request.session['username']
    del request.session['id']
    return render( request, "login_register_view.html")

def history(request) :
    if 'username' not in request.session:
        return render(request,'index.html',{'username':'','user_id':''})
    else:
        user_id = request.session[ 'id' ]
        account = Accounts.objects.get(user_id=user_id)
        order = Order.objects.filter(user_id=account.user_id)
        # results = Order.objects.extra(tables=["Top_Up_Type"],
        #                   where=["""Top_Up_Type.top_up_type_id = Order.top_up_type_id"""])
        user_wallet = User_Wallet.objects.get(user_id=user_id);

        return render(request,'history.html',{'order':order ,'username':request.session[ 'username' ],'user_id':request.session[ 'id' ],'email':request.session[ 'email'],'wallet':user_wallet})

def get7code() :
    tmp = ""
    i = 0
    while i < 30 :
        tmp = tmp + str( random.randint( 0, 9 ) )
        i += 1
    return tmp

def getSteamCode() :
    tmp = ""
    i = 1
    while i <= 15 :
        if random.randint( 0, 1 ) == 1 :
            tmp = tmp + str( chr( random.randint( 97, 122 ) ) )
        else :
            tmp = tmp + str( random.randint( 0, 9 ) )
        if i % 5 == 0 and i != 15 :
            tmp = tmp + str( "-" )
        i += 1
    return tmp

def getPSCode() :
    tmp = ""
    i = 1
    while i <= 12 :
        if random.randint( 0, 1 ) == 1 :
            tmp = tmp + str( chr( random.randint( 97, 122 ) ) )
        else :
            tmp = tmp + str( random.randint( 0, 9 ) )
        if i % 4 == 0 and i != 12 :
            tmp = tmp + str( "-" )
        i += 1
    return tmp

def edit(request):
    new_username = request.POST['username']
    new_email = request.POST['email']
    Accounts.objects.filter( user_id=request.session['id']).update(username=new_username,email=new_email)
    user_id = request.session[ 'id' ]
    account = Accounts.objects.get(user_id=user_id)

    request.session[ 'username' ] = account.username
    request.session[ 'id' ] = account.user_id
    request.session[ 'email' ] = account.email

    return history(request)

def topup(request) :
    code_id = request.POST['code_id']
    value = Truemoney.objects.filter(card_id=code_id)
    current_value = User_Wallet.objects.get(user_id=request.session['id']);
    new_value = int(value[0].value)+int(current_value.money)

    User_Wallet.objects.filter( user_id=request.session['id']).update(money=new_value)

    return history(request)

def mainforum(request):
    if 'username' not in request.session:
        return render(request,'index.html',{'username':'','user_id':''})
    else:
        question_list = Question.objects.all()
        account_list = Accounts.objects.all()
        return render(request,'mainforum.html',{'username':request.session['username'],'question_list':question_list,'account_list':account_list})

def postQuestion(request):
    if 'username' not in request.session:
        return render(request,'mainforum.html',{'username':'','user_id':''})
    else:
        question_subject = request.POST['question_subject']
        question_detail = request.POST['question_detail']
        user_id = request.session[ 'id' ]
        question = Question.objects.create(question_subject=question_subject,question_detail=question_detail,user_id=user_id,question_date=datetime.now())
        question.save()
        return HttpResponseRedirect(reverse('main:mainforum'))

def viewQuestion(request):
    if 'username' not in request.session:
        return render(request,'index.html',{'username':'','user_id':''})
    else:
        account_list = Accounts.objects.all()
        try :
            print ('------------------',request.POST['select_question'])
            question = Question.objects.get(question_id=request.POST['select_question'])
            print('1111')
            reply = Reply.objects.filter(question_id=question.question_id)
            print('2222')
        except KeyError :
            question = Question.objects.get(question_id=request.session['replyQuestion'])
            reply = Reply.objects.filter(question_id=question.question_id)
            del request.session['replyQuestion']
        return render(request,'viewQuestion.html',{'username':request.session['username'],'select_question':question,'reply_list':reply,'account_list':account_list})


def replyQuestion(request):
    if 'username' not in request.session:
        return render(request,'mainforum.html',{'username':'','user_id':''})
    else:
        print ('------------------reply' , request.POST['replyQuestion'])
        question = Question.objects.get(question_id=request.POST['replyQuestion'])
        reply_detail = request.POST['reply_question']
        user_id = request.session[ 'id' ]
        reply = Reply.objects.create(reply_detail=reply_detail,question_id=question.question_id,user_id=user_id,reply_date=datetime.now())
        reply.save()
        request.session['replyQuestion'] = request.POST['replyQuestion']
        return HttpResponseRedirect(reverse('main:viewQuestion'))

def deleteQuestion(request):
    if 'username' not in request.session:
        return render(request,'mainforum.html',{'username':'','user_id':''})
    else:
        question = Question.objects.get(question_id=request.POST['deleteQuestion'])
        Reply.objects.filter(question_id=question.question_id).delete()
        Question.delete(question)
        request.session['deleteQuestion'] = request.POST['deleteQuestion']
        return HttpResponseRedirect(reverse('main:mainforum'))
