# -*- coding: utf-8 -*-
# Generated by Django 1.10.2 on 2016-11-09 23:32
from __future__ import unicode_literals

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('main', '0007_merge_20161109_2258'),
    ]

    operations = [
        migrations.AddField(
            model_name='order',
            name='top_up_type_id',
            field=models.IntegerField(default=1),
            preserve_default=False,
        ),
    ]
